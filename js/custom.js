// scroll to anchor link in osha training pages
// select all links with hashes
$('a[href*="#register"]')
    .not('[href="#"]')
    .not('[href="#0"]') // Remove links that don't actually link to anything
    .click(function(event) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) { // On-page links
            var target = $(this.hash); // Figure out element to scroll to
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                event.preventDefault(); // Only prevent default if animation is actually gonna happen
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1500, function() {
                    var $target = $(target); // Callback after animation, must change focus!
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                    } else {
                        $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    }
                });
            }
        }
});

// check if route url is the free diagnostic form
isFreeDiagnosticUrl = function() {
    var $fullUrl = window.location.href;
    return ($fullUrl.indexOf('free-diagnostic') > -1);
};

// show/hide free diagnostic form only if the user requested from the homepage + check session storage
var $diagnoticForm = $("#diagnosticForm"), $diagnosticMessage = $("#diagnosticMessage");
if(typeof(Storage) !== "undefined" && isFreeDiagnosticUrl()) {
    if (sessionStorage.freeDiagnostic == 1) {
        $diagnosticMessage.hide();
        $diagnoticForm.removeClass('no-display').show();
    } else {
        $diagnoticForm.hide();
        $diagnosticMessage.show();
    }
} else {
    sessionStorage.freeDiagnostic = 2;
}