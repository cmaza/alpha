<footer id="footer">
    <div class="footer-content">
        <div class="container">
            <div class="row gap-y">
                <div class="col-xl-4 col-lg-4 col-md-12">
                    <div class="widget clearfix">
                        <p><a href="<?php echo basePathUrl();?>"><img src="<?php echo basePathUrl();?>images/Atlas-Logo-Reversed.png" alt="logo"></a></p>
                        <div class="clearfix m-b-40"></div>
                        
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-3">
                    <div class="widget">
                        <h4>Navigation</h4>
                        <ul class="list">
                            <li><a href="<?php echo basePathUrl();?>">Home</a></li>
                            <li><a href="<?php echo basePathUrl();?>services">Services</a></li>
                            <li><a href="<?php echo basePathUrl();?>about-us">About Us</a></li>
                            <li><a href="<?php echo basePathUrl();?>contact-us">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-3">
                    <div class="widget">
                        <ul class="list">
                            <li class="menu-icon white-text">
                                <i class="fa fa-map-marker-alt fa-flip-horizontal"> </i>  &nbsp;&nbsp;103 E. Fourth St. Rochester, MI 48307
                            </li>
                            <li class="menu-icon">
                                <a href="tel:2484020075"><i class="fa fa-phone fa-flip-horizontal"></i>248.402.0075</a>
                            </li>
                            <li class="menu-icon">
                                <a href="tel:2484020080"><i class="fa fa-fax fa-flip-horizontal"></i>248.402.0080</a>
                            </li>
                            <li class="menu-icon">
                                <a href="mailto:info@atlasproserv.com"><i class="fa fa-envelope fa-flip-horizontal"></i>info@atlasproserv.com</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-content">
        <div class="container">
            <div class="copyright-text text-center">Copyright © <?php echo date('Y'); ?> Atlas Professional Services L.L.C. | <a href="<?php echo basePathUrl();?>privacy-policy">Privacy Policy</a> | <a href="<?php echo basePathUrl();?>terms-of-use">Terms of use</a></div>
        </div>
    </div>
</footer>