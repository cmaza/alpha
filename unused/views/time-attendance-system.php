<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/TLM.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Time & Attendance System</h1>
            <span>Track, Understand, and Manage Your Workforce Better</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Time-Img1.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Real-time Solutions</h4>
                    <div class="inside-spacer"></div>
                    <p>VensureHR’s cloud-based time and attendance software ensures every minute of your employees’ time is tracked at the lowest possible cost.
                        Secure fingerprint terminals combined with mobile solutions for time and expense tracking and geofencing put you back in the driver’s seat.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Ease of Use</h4>
                    <p>A truly intuitive system built with users in mind and complete with the tools needed to get the job done.</p>
                    <h4>Step-by-Step Set-up</h4>
                    <p>Let VensureHR’s team walk you through the set-up process one step at a time.</p>
                    <h4>Customized Training</h4>
                    <p>Your system is customized to you, so why should your training be generic? We offer systems training tailored to your specific business rules and policies.</p>
                    <h4>Dedicated Support</h4>
                    <p>As you ease into the new application, a trained VensureHR representative will be available for any questions you may have.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Time-Phone.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-50"></div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Time-Fingerprint.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>Industry-Specific Solutions</h4>
                <p>From robust scheduling and complex calculations to reporting and tele-punch, VensureHR offers customized solutions to fit your business needs.</p>
                <ul class="list-unstyled m-l-20">
                    <li class="p-t-5 p-b-5">User-defined fields</li>
                    <li class="p-t-5 p-b-5">PTO or leave of absence requests</li>
                    <li class="p-t-5 p-b-5">Benefit accruals</li>
                    <li class="p-t-5 p-b-5">Drag and drop scheduling</li>
                </ul>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Are You Ready to Grow Your Business With VensureHR?</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
</section>