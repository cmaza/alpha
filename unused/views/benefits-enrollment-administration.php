<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-Benefits-Enrollment.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Benefits Enrollment & Administration</h1>
            <span>Let Us Handle the Paperwork</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Benefits-Enrollment-Simplified.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>Simplified Enrollment and Administration</h4>
                <div class="inside-spacer"></div>
                <p>Whether your HR team is working on enrolling a new employee or managing your annual open enrollment, <strong>benefit administration</strong> has never been easier.
                    VensureHR gives you the ability to outsource tasks related to establishing, maintaining, and managing your organization’s benefits package(s). From personalized, professional support to administering the entire process, our team is available to help your business from plan selection to annual renewals.</p>
                <p class="p-t-20"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey">
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="row">
                    <div class="col-lg-12">
                        <h4>Unique Plans and Coverage Options</h4>
                        <p class="m-t-20">Today’s industries require unique plans and coverage options to keep up with federal, state, and local requirements.</p>
                    </div>
                    <div class="col-lg-12">
                        <h4>Vfficient from VensureHR</h4>
                        <p class="m-t-20">A single, streamlined platform that helps your business manage everything from benefits and employee onboarding to billing and payroll processing.</p>
                    </div>
                    <div class="col-lg-12">
                        <h4>Beyond Healthcare</h4>
                        <p class="m-t-20">VensureHR has done the legwork to help your organization obtain <strong>employee benefits</strong> beyond standard healthcare, vision, and dental.</p>
                    </div>
                    <div class="col-lg-12">
                        <h4>Sensitive Information</h4>
                        <p class="m-t-20">Accurate management of sensitive employee HIPPA information.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Benefits-Enrollment-Coverage.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">HR Solutions That Deliver the Freedom to Succeed</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
