<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/Vensure-HR-Partners-Broker.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">PEO Broker</h1>
            <span>Partnerships Devoted to Building Successful Long-Term Relationships</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/VensureHR-PEO-Brokers.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>Building Quality Relationships</h4>
                <div class="inside-spacer"></div>
                <p>Partnering with VensureHR means putting the time and energy into meaningful relationships. VensureHR is eager to collaborate with brokers and agencies
                    who share this approach to business. If you’re ready to take your agency to new heights, VensureHR is ready to join your journey of success as we build this partnership.</p>
                <p class="p-t-20"><a href="#modalBecomeAPartnerToday" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-5 p-r-40">
                <h4>High Volume Submission Solution</h4>
                <div class="inside-spacer"></div>
                <p>At VensureHR you’ll work directly with an assigned underwriter available to answer your workers' compensation questions from proper class codes to
                    program eligibility for faster turnaround, better communication and happier clients.</p>
                <p class="p-t-10">Discuss your agency and needs with VensureHR to see if we can help take your agency to the next level.</p>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Applicant-Tracking-Right-Track.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey">
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/VensureHR-PEO-Broker-Competitive-Comissions.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-4">
                <h4>Competitive Commissions</h4>
                <p class="m-t-20">VensureHR provides non-depreciating residual commissions to partners for the life of the client.
                    These life-long commissions build up year-over-year as VensureHR focuses heavily on client retention through client satisfaction.</p>
            </div>
            <div class="col-lg-4">
                <h4>Higher Approval Rates</h4>
                <p class="m-t-20">With eight workers' compensation programs, two master health programs, and access to open market health carriers
                    VensureHR helps gain approvals for some of the hardest to place business.</p>
            </div>
            <div class="col-lg-4">
                <h4>Full Suite of Services</h4>
                <p class="m-t-20">From workers’ compensation and payroll administration to employee benefices and risk management; VensureHR’s services come with
                    a single-sign-on portal access to our human capital management (HCM) technology designed to simplify nearly every aspect of the client's workday.</p>
            </div>
        </div>
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-4">
                <h4>Vensure Marketplace</h4>
                <p class="m-t-20">VensureHR offers a wider range of ancillary and free-to-enroll employee-focused products than any other PEO or HR
                    administrator on the market. Explore the <a href="<?php echo basePathUrl();?>peo-services/marketplace" class="internal">Vensure Marketplace</a>.</p>
            </div>
            <div class="col-lg-4">
                <h4>Prospect Opportunities</h4>
                <p class="m-t-20">VensureHR provides select partners a steady stream of prospect opportunities to work.</p>
            </div>
            <div class="col-lg-4">
                <h4>Proactive Transparent Communication</h4>
                <p class="m-t-20">Stay up-to-date with new and active clients with dedicated partner representation to provide ongoing support.</p>
            </div>
        </div>
        <div class="section-spacer-50"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Are You Interested in a Partnership That Grows Your Business & Supports Your Success?</h4>
                <p class="m-t-60 text-center">
                    <a href="#modalBecomeAPartnerToday" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
