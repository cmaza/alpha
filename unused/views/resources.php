<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-Resources.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Resources</h1>
            <span>Blogs, Training, Events, and More</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-right.png" />
    </div>
</section>

<section>
    <div class="container">
        <div id="portfolio" class="grid-layout portfolio-4-columns" data-margin="20">
            <div class="portfolio-item no-overlay img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="<?php echo basePathUrl();?>resources/blog"><img src="<?php echo basePathUrl();?>images/Resource-Listing-Blog.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="portfolio-item no-overlay img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="<?php echo basePathUrl();?>resources/what-is-a-peo"><img src="<?php echo basePathUrl();?>images/Resource-Listing-What-PEO.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="portfolio-item no-overlay img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="<?php echo basePathUrl();?>client-center"><img src="<?php echo basePathUrl();?>images/Resource-Listing-Client-Center.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="portfolio-item no-overlay img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="<?php echo basePathUrl();?>resources/events"><img src="<?php echo basePathUrl();?>images/Resource-Listing-Events.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-20"></div>
        <div id="portfolio" class="grid-layout portfolio-2-columns" data-margin="20">
            <div class="portfolio-item no-overlay img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="<?php echo basePathUrl();?>resources/general-industry-training"><img src="<?php echo basePathUrl();?>images/Resource-Listing-Training-General.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="portfolio-item no-overlay img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="<?php echo basePathUrl();?>resources/construction-training"><img src="<?php echo basePathUrl();?>images/Resource-Listing-Training-Construction.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-50"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">See How Vensure Can Help Improve Your Business</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
</section>
