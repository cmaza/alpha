<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-Careers.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Careers</h1>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-right.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Come Work With Us!</h4>
                <div class="inside-spacer"></div>
                <p>There are a lot of reasons to join us here at Vensure. Perhaps, it’s our insane passion for the work or it’s our independence.
                    Either way, share your energy and camaraderie and become inspired to break new ground with us. We’re constantly seeking talented professionals,
                    who are just as dedicated and passionate as we are. If you have a desire to excel and a strong work ethic, take a look at Vensure’s
                    career opportunities by clicking the link below.</p>
                <p class="m-t-30 text-center">
                    <a href="https://vensure-employer-services.hiringthing.com/" target="_blank" class="btn btn-rounded btn-light">View Openings</a>
                </p>
            </div>
        </div>
    </div>
</section>