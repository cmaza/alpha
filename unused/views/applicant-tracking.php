<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-Application-Tracking.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Applicant Tracking</h1>
            <span>Simplify Your Hiring Process</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Applicant-Tracking-Right-Track.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>Get on the Right Track</h4>
                <div class="inside-spacer"></div>
                <p><strong>Applicant tracking</strong> with VensureHR means you can finally attract top talent for any of your open job opportunities.
                    From ensuring the candidates have the right experience and meet the requirements, to actively managing the hiring process, VensureHR will keep you smiling.</p>
                <p class="p-t-20"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey">
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Applicant-Tracking-Custom-Screening.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-3">
                <h4>Career Portal</h4>
                <p class="m-t-20">Automatically post jobs to your favorite social platforms or recruiting websites, track engagement, and actively manage through real-time updates.</p>
            </div>
            <div class="col-lg-3">
                <h4>Custom-built</br>Screening Process</h4>
                <p class="m-t-20">Implement the same screening process and candidate ranking that you currently use in the portal.</p>
            </div>
            <div class="col-lg-3">
                <h4>Dashboard</br>Overview</h4>
                <p class="m-t-20">From email triggers to text capabilities, hiring managers can answer candidate questions, schedule interviews, or initiate the hiring process.</p>
            </div>
            <div class="col-lg-3">
                <h4>Workflow Tools</h4>
                <p class="m-t-20">Easy access tools for managers include the ability to set-up reminders and notifications in addition to 24/7 access to analytics and reports.</p>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">HR Solutions That Deliver the Freedom to Succeed</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
