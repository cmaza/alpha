<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-center free-diagnostic m-b-40">Get a Free, Custom Diagnostic for Your Business!</h4>
            </div>
            <div id="diagnosticForm" class="col-md-6 offset-md-3 no-display">
                <form class="form-free-diagnostic" novalidate action="<?php echo basePathUrl();?>form-send/free-diagnostic" role="form" method="post">
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <input type="text" name="full_name" class="form-control form-input-home" placeholder="Full Name*" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <input type="text" name="phone_number" class="form-control form-input-home" placeholder="Phone Number*" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <input type="text" name="company" class="form-control form-input-home" placeholder="Company*" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <input type="text" name="number_employees" class="form-control form-input-home" placeholder="Number of Employees*" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <button type="submit" id="free-diagnostic-bottom" class="btn btn-rounded btn-light">Get Started</button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="diagnosticMessage" class="col-md-12 text-center">
                <p>Please fill out the form on the previous page.</p>
            </div>
        </div>
    </div>
</section>

