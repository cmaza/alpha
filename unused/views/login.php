<div id="portals" class="d-md-flex h-md-100 align-items-center">
    <div class="col-md-6 p-0 background-grey h-md-100">
        <div class="d-md-flex align-items-center h-100 p-5 text-left">
            <div class="left-side">
                <a href="<?php echo basePathUrl();?>"><img src="<?php echo basePathUrl();?>images/VensureHR_RGB_logo.png" /></a>
                <div class="section-spacer-10"></div>
                <h1 class="m-b-15 m-t-5">Select Your Login Portal</h1>
                <p><a href="<?php echo basePathUrl();?>" class="internal">Back to Home</a> | <a href="<?php echo basePathUrl();?>contact" class="internal">Contact Us</a></p>
            </div>
        </div>
    </div>
    <div class="col-md-6 p-0 bg-white h-md-100">
        <div class="d-md-flex align-items-center h-md-100 p-5">
            <div class="right-side">
                <h2>Client</h2>
                <div class="buttons-links">
                    <a href="https://vns.prismhr.com/vns/dbweb.asp?dbcgm=1" target="_blank" class="btn">Manager</a>
                </div>
                <div class="buttons-links">
                    <a href="https://hrpyramid.net/vns/SuperUserLogin" target="_blank" class="btn">Super User</a>
                </div>
                <div class="buttons-links">
                    <a href="https://smart.vensure.com/Login.aspx" target="_blank" class="btn">SMART</a>
                </div>
                <div class="section-spacer-10"></div>
                <h2>Employee</h2>
                <div class="buttons-links">
                    <a href="https://vns-ep.prismhr.com/#/auth/login" target="_blank" class="btn">Login</a>
                </div>
                <div class="buttons-links">
                    <a href="https://vns-ep.prismhr.com/#/auth/register" target="_blank" class="btn">Registration</a>
                </div>
            </div>
        </div>
    </div>
</div>