<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-MDLIVE-Hero.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Telemedicine by MDLIVE </h1>
            <span></span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/VensureHR-mdlive-video-thumbnail.jpg" alt=""></a>
                            </div>
                            <div class="portfolio-description">
                                <a data-lightbox="iframe" href="https://www.youtube.com/watch?v=oAnygVlJ7ys">
                                    <i class="fas fa-play"></i>
                                </a>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <img src="<?php echo basePathUrl();?>images/mdlive_owler_20190815_113845_original.png" alt="">
                    <div class="inside-spacer"></div>
                    <p>Vensure Employer Services has responded to the coronavirus (COVID-19) outbreak by extending its telemedicine services, MDLIVE, at no cost to clients, employees, and their family members for the next 90 days. This will allow nearly 300,000 of our extended-Vensure family and their families to speak with a licensed, board-certified medical professional from a safe place during this period of social distancing.</p>
                    <p>“With the current unprecedented strain on healthcare services, facilities and healthcare professionals are overwhelmed. We want to assure our clients and their employees have access to the care they need,” says Alex Campos, CEO of Vensure Employer Services. “Everyone is focused on COVID-19, but there are also all the other routine healthcare needs of the people we serve.”</p>
                    <p>Vensure Employer Services encourages clients, employees, and family members to take advantage of this telemedicine service. For more information on Vensure Employer Services benefits and services, please contact MDLIVE.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>MDLIVE</h4>
                    <div class="inside-spacer"></div>
                    <p>During the ongoing COVID-19 crisis, social distancing and self-quarantining is critical to reducing the spread of the novel coronavirus, as well as keeping you and your loved ones safe and infection free. Unfortunately, the new protocols put in place significantly disrupt our lives. With so many businesses closed or temporarily shut down and our healthcare system more strained than ever before, it’s nearly impossible to do things as simple as running errands or visit your doctor. However, Vensure Employer Services is providing clients, employees, and their families with a safer alternative during this trying time.</p>
                    <p>As a client or employee of Vensure Employer Services, you and your family have access to telemedical services via MDLIVE.</p>
                    <p>MDLIVE offers users* a telemedicine alternative to risking a trip to visit your doctor; it allows you to receive care from a board-certified doctor’s office without leaving the comfort and safety of your own home. Doctors are available by phone, secure video, of the MDLIVE app 24 hours a day, any day, including both weekends and holidays. MDLIVE’s doctors can help with many routine conditions and send prescriptions to your local pharmacy as medically required. MDLIVE saves you the time and money of visiting emergency rooms and urgent care, all while keeping you and your family safe during these uncertain times.</p>
                    <p></p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/VensureHR-MDLIVE-Doctor.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-50"></div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/VensureHR-MDLIVE-Computer.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>How to Join</h4>
                    <div class="inside-spacer"></div>
                    <p>Vensure Employer Services clients, please contact your individual benefits managers* to get started today. Starting April 1st, nearly 300,000 of our extended-Vensure family and their families to speak with a licensed, board-certified doctors for 90 days, free of charge.</p>
                    <p>Vensure Employer Services employees can visit MDLIVE.com/VES and use their Vensure.com emails to sign up. Download the free MDLIVE app to your smartphone or tablet today and sign up for free. Visit MDLIVE online at MDLIVE.com/VES or call (888) 983-4857. Members can text “VES” to 635-483.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-6">
                			<a href="<?php echo basePathUrl();?>PDFs/RTTM Welcome Flyer VES.pdf" target="_blank">
                        	<img alt="MDLIVE Welcome Flyer" src="<?php echo basePathUrl();?>images/RTTM-Thumbnail.jpg.jpg"{></a>
                        	<div class="section-spacer-10"></div>
                        	<a href="<?php echo basePathUrl();?>PDFs/RTTM Welcome Flyer VES.pdf"  target="_blank" class="btn btn-rounded btn-light">Download MDLIVE Welcome Flyer</a>
                        	<div class="section-spacer-30"></div>
            			 </div>
            <div class="col-lg-6">
                			<a href="<?php echo basePathUrl();?>PDFs/Ves Sophie Inst Flyer 20-J123B7-1_doc_highres-1.pdf" target="_blank">
                        	<img alt="MDLIVE Chatbot Guide" src="<?php echo basePathUrl();?>images/VES-Inst-Thumbnail.jpg.jpg"{></a>
                        	<div class="section-spacer-10"></div>
                        	<a href="<?php echo basePathUrl();?>PDFs/Ves Sophie Inst Flyer 20-J123B7-1_doc_highres-1.pdf"  target="_blank" class="btn btn-rounded btn-light">Download MDLIVE Chatbot Guide</a>
                        	<div class="section-spacer-30"></div>
            			 </div>
        </div>
    </div>
    <div class="section-spacer-50"></div>
    
</section>

