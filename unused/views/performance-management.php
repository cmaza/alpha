<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/PerfMgmt.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Performance Management</h1>
            <span>Enable Employees to Perform at Their Best</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Recruiting-All-Inclusive.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>Creating a Culture of High Performance</h4>
                <div class="inside-spacer"></div>
                <p>Nearly 15% of companies can reduce turnover by simply providing valuable employee feedback on a regular basis. Inspire and engage with your employees
                    to create a culture of high performance aimed at establishing role-based goals and tracking progress.</p>
                <p class="p-t-20"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey">
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Applicant-Tracking-Direct-Communication.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-3">
                <h4>Flexible</br>Configuration</h4>
                <p class="m-t-20">A user-friendly interface that supports a variety of review formats.</p>
            </div>
            <div class="col-lg-3">
                <h4>Automated</br>Reminders</h4>
                <p class="m-t-20">Stay in the know and never miss a deadline with timely review completion reminders.</p>
            </div>
            <div class="col-lg-3">
                <h4>Dashboard</br>Overview</h4>
                <p class="m-t-20">Milestone achievements and overdue appraisal information at your fingertips.</p>
            </div>
            <div class="col-lg-3">
                <h4>Coaching</br>Resources</h4>
                <p class="m-t-20">Tools available for managers, in addition to suggestions for personal development.</p>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">HR Solutions That Deliver the Freedom to Succeed</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
