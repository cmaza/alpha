<section>
    <div class="container">
        <div class="section-spacer-30"></div>
        <div class="row text-center">
            <div class="col-lg-12">
                <h2>Thank You for Your Submission</h2>
                <p><a href="/">Return to Homepage</a></p>
            </div>
        </div>
        <div class="section-spacer-30"></div>
    </div>
</section>

