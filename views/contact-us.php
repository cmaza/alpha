<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/Atlas-Contact-Us.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Contact Atlas Professional Services</h1>
        </div>
    </div>
</section>

<section id="page-content" class="sidebar-right">
    <div class="section-spacer-40"></div>
    <div class="container">
        <div class="row">
            <div class="content col-lg-8">
                <h3>For any further questions or for a no obligation quote, please fill out the form below.</h3>
                <div class="section-spacer-20"></div>
                <iframe src="https://go.vensure.com/l/656143/2020-04-13/2bx3g8" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
                </div>
            <!-- Sidebar -->
            <div class="sidebar contact col-lg-4">
                <div class="background-light sidebar">
                    <div class="section-spacer-10"></div>
                <div class="section-spacer-8"></div>
                <div class="icon-box1 medium color">
                    <div class="icon"><i class="fas fa-map-marker-alt contact"></i></div>
                    <h5 class="training-calendar contact">103 E. Fourth St.</h5>
                    <h5 class="training-calendar contact">Rochester, MI 48307</h5>
                </div>
                <div class="section-spacer-8"></div>
                <div class="icon-box1 medium color">
                    <div class="icon"><i class="fas fa-phone fa-rotate-90 contact"></i></div>
                    <h5 class="training-calendar contact">248.402.0075</h5>
                </div>
                <div class="section-spacer-8"></div>
                <div class="icon-box1 medium color">
                    <div class="icon"><i class="fas fa-fax contact"></i></div>
                    <h5 class="training-calendar contact">248.402.0080</h5>
                </div>
                <div class="section-spacer-8"></div>
                <div class="icon-box1 medium color">
                    <div class="icon"><i class="fas fa-envelope contact"></i></div>
                    <h5 class="training-calendar contact">info@atlasproserv.com</h5>
                </div>
                <div class="section-spacer-10"></div>
            </div>
            </div>
        </div>
</section>