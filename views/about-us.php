<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/Atlas-About-Hero.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">About Atlas Professional Services</h1>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Atlas-About-Services.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Atlas Professional Services</h4>
                    <div class="inside-spacer"></div>
                    <p>Atlas is an industry-leading PEO service provider specializing in equipping employers with best-in-class HR outsourcing services and support. As a pioneer in human resource services, we offer a comprehensive package of PEO solutions for all businesses.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-10"></div>
</section>

<section class="background-grey p-t-50">
    <div class="section-spacer-40"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Atlas is Your HR Outsourcing Partner</h4>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                         <p>Atlas has positioned itself to take advantage of the rapidly expanding PEO industry by developing the systems necessary to acquire and serve its growing clientele. With state-of-the-art, custom-designed software for payroll preparation and administering employee benefits, Atlas can quickly handle hundreds of employees with minimal in-house personnel.</p>
                         <p>Most business owners will readily admit that their businesses would operate more efficiently and profitably if their time and energy was not preoccupied with paperwork, administration, and regulations.</p>
                         <p>Atlas fills vital needs for businesses by providing PEO solutions for employers through co-employment arrangements. Atlas will assume all of the human resource administration duties for you as your PEO provider allowing you to focus on revenue-generating tasks.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <p>Through co-employment, Atlas assumes all of the responsibilities of HR administration, enabling us to take on tasks for our clients’ businesses, like paying taxes and administering human resource services. As a result, our clients can run their businesses and continue to hire and manage employees.</p>
                         <p>In addition to eliminating all payroll, government reporting, insurance, and administration concerns, Atlas can negotiate better rates and terms for employee benefits. Business owners are better able to attract and retain the most qualified personnel.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-12">
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-10"></div>
</section>

<section>
    <div class="container">
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Atlas-About-PEO.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>What is a PEO?</h4>
                    <div class="inside-spacer"></div>
                    <p>A PEO, or professional employer organization, is a co-employment arrangement in which the PEO company manages the time-consuming but necessary administrative tasks while business owners can refocus on their core business, increasing efficiency and profits for the business.</p>
                    <p>Co-employment involves two companies that carry the business rights and obligations as employers. One company serves as the core business and the other company supports the core business as a co-employer. The business handles day-to-day functions and employees’ jobs, while the co-employer (a PEO) manages HR and personnel-related functions, services, and benefits.  </p>
                    <p>Co-employment arrangements help clients transfer many of the employee-related risks and responsibilities to the PEO, such as wage and employment tax, workers’ compensation coverage and claims management, and other HR and employee-related employer services.  The co-employment relationship allows the PEO offer their clients’ employees superior benefits and alternative support options.  </p>
                </div>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">See How Atlas Can Help Improve Your Business</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
        <div class="section-spacer-40"></div>
    </div>
</section>