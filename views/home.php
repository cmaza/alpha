
    <section class="fullscreen home" data-bg-parallax-home="<?php echo basePathUrl();?>images/home/Atlas-Home-Hero.jpg">
        <div class=""></div>
        <div class="container-wide">
            <div class="container-fullscreen">
                <div class="text-middle">
                    <div class="heading-text col-lg-6">
                        <h1><span>Partner With Us for Your HR Outsourcing Solutions</span></h1>
                        <p>Our goal is to help you find more time to focus on business needs!</p>
                        <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's get Started</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h2>HR Outsourcing Solutions</h2>
                <span class="lead sub-header">Atlas Professional Services is an Expert at Managing Complex Businesses</span>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <div>
                        <h4>Who We Are</h4>
                        <div class="inside-spacer"></div>
                        <p>Atlas Professional Services is a professional employer organization (PEO) providing human resource management, payroll processing, benefits administration, an HRIS system, workers' compensation, and risk management services.</p>
                        <p>Clients can improve overall productivity, reduce employee turnover, and increase profitability across their businesses. Our tailored solutions help businesses meet objectives and deliver outstanding results to the company and employees.</p>
                        <p class="m-t-40 m-b-40"><a href="<?php echo basePathUrl();?>/about-us" class="btn btn-rounded btn-light">Learn More</a></p>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/Atlas-Home-Who-We-Are.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-10"></div>
    </section>
    
    <section class="background-grey p-t-50">
    <div class="section-spacer-40"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Atlas-Home-Specializing-in-PEO.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Specializing in PEO Services</h4>
                    <div class="inside-spacer"></div>
                    <p>By partnering with Atlas, companies gain access to our team of service professionals who specialize in human resources, payroll, workers' compensation, risk management, unemployment, benefits administration, and employment-related compliance.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-10"></div>
    </section>
    
    <section>
        <div class="section-spacer-20"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div>
                        <h4>More Time, More Money</h4>
                        <div class="inside-spacer"></div>
                        <p>Atlas will help eliminate all administrative tasks that diminish clients’ ability to focus on growing their businesses. Additionally, Atlas has a team of seasoned professionals to customize business solutions and negotiate services on clients’ behalf.</p>
                        <p>Atlas is an industry-leading PEO specializing in providing employers with best-in-class HR outsourcing services and support.</p>
                        <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/Atlas-Home-More-Time.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="section-spacer-10"></div>
    </section>
    <!-- end: Content -->